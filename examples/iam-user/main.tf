terraform {
  required_version = ">= 0.12.6"
}

provider "aws" {
  region = "eu-west-1"
}

//  IAM user, login profile and access key
module "iam_user" {
  source = "../../modules/iam-user"

  name          = "fabio"
  force_destroy = true

  // User "test" has uploaded his public key here - https://keybase.io/test/pgp_keys.asc
  pgp_key = "keybase:test"

  password_reset_required = false

  //  SSH public key
  upload_iam_user_ssh_key = true

  ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDZpU5BlX+4AHgOuV6qxw5XBNx0+wrZe6GN3eD5tTAVY9GQpGUXCTHWCrIpQBrOtlRVec/+3PEtn5S3qSJnVsFFrDms3rt+G8LOfkMyKgeWXo6Tjevjy+SI8yv4ggYAvWcjjHAmgLvOMlAUKtZGJlYOw8cgOLV/nx74oCXJvoO8/L+ALpPbK0l63Di9tFbggLOMynqMc86/fqRk3Pf1YcgENzAYBK0/ObjkmIl4XEEx9dBmfcq6OjitNfa+CL3FelSipgj4hn4MNpPG7h3CzAUbjNQfN/HQT9g6mqWmMj3uESyYPEhPPC6IMEyX89qWcohVqT/IIhWbNrM7tyTm+wPvvvMCywprwGOmKNVkqJgZc2OgG/XsbAXzOdywssi/k4aInnOFFNfpQbnHaKmaywOhLixIllPP6FVHEh9/PFxzCzIeL6wBQRZb6fcf0f9AuiFrd1Jk8aHiBBeN/SPAHU6YBuJ1iOh5UanJHlkGqC3afMS6JdJS7t5M1Yapm87erFVW1Y5UjwJrUgTKMRS3aqnOFhJ73QXPD0wkcf6xSwI6HW57mnEJ8NfQjypQvLGtEJwr6Bcusw+2lsTtO919bUatEKey5I7fzrORHH5ebM/8D1XtPn03Mdl9zzIYQKVua1dB5OvL14x2qBUPMmmdMYjTOcBgKVqYt+TVbDL2X1bpLQ== fabio.coelho@agilitynetworks.com.br"
}


// IAM user without pgp_key (IAM access secret will be unencrypted)
module "iam_user2" {
  source = "../../modules/iam-user"

  name = "fabio2"

  create_iam_user_login_profile = false
  create_iam_access_key         = true
}